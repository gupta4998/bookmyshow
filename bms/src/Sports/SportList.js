import React from 'react';
import axios from 'axios';
import "semantic-ui-css/semantic.min.css";
import './SportList.css';
import Sport from './Sport';
import basePath from '../api/basePath';



class SportList extends React.Component {

    state = { sport_info: [],
       // filterGenre:
           // movie_info2: [],
    };
    
    componentDidMount() {
    this.getSportInfo();
    }

   getSportInfo = async () => {
        const sportresponse = await basePath({
            method: "get",
            url: '/api/sports',
            withCredentials: true
        })
        .then(() => {
           basePath({
               method: "get",
               url: 'api/sports/?size=compact&limit=12'
           })
            .then(res => {
                const sports = res.data.slice(0, 12)
               // const movies2 = res.data.slice(6, 12)
                this.setState({
                    sport_info: sports,
                //    movie_info2: movies2
                });
            });
        });
    }

    render() {
        return(
            <div className="sportList">
                <div className="sportList_release">
                    <img className="sportList_icon" src="../assets/img/star.svg" alt="icon - star" />
                    <div className="sportList_release_text my-4"> Sports Ticket Booking </div>
                    <img className="sportList_icon" src="../assets/img/star.svg" alt="icon - star" />
                </div>
                <Sport setSportId={this.props.setSportId} sportinfo = {this.state.sport_info}/>
                {/* <div className="movieList_today">
                    <div>Today</div>
                </div>
                <Movie setMovieId={this.props.setMovieId} movieinfo = {this.state.movie_info2}/> */}
            </div>   
        );
    }
}

export default SportList;