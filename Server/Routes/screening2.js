const express = require('express');
const router = express.Router();
const {Screening2, validateScreening2} = require('../models/screening2');
const {ScreeningRoom2, validateScreeningRoom2} = require('../models/screeningRoom2');
const auth = require('../middleware/auth');
const cookieParser = require('cookie-parser');

router.use(cookieParser())

router.get("/", async (req, res) => {
    const screenings2 = await Screening2.find()

    res.send(screenings2);
});

router.get("/:id", async (req, res) => {
    const screening2 = await Screening2.findById(req.params.id);
    if (!screening2) return res.status(400).send('No screening exists under given ID.');

    res.send(screening2);
});

router.post("/",  async (req, res) => {
    const { error } = validateScreening2(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const screening2 = new Screening2(req.body);
    const screeningRoom2 = await getScreeningRoom2(req.body.screeningRoomId);
     console.log(screeningRoom2.seats);
    const arr = Object.entries(screeningRoom2.seats)
    console.log(Object.entries(screeningRoom2.seats))

    const myHeadHurts =[]
    
    
    for(let i = 0; i < arr.length; i++) {
        for(seat of arr[i][1]) {
            if(arr[i][0]==="F") break;
            if(Number.parseInt(arr[i][0]) > 10) continue;
            myHeadHurts.push({
                row: arr[i][0],
                seat: seat,
                isOccupied: false,
                userID: ""
            });
        }
    };

    screening2.seats = myHeadHurts;
    screening2.city = screeningRoom2.city;
    screening2.ground=screeningRoom2.ground;

        await screening2.save();
        res.send(screening2);
});

router.delete("/:id",  async (req, res) => {

    const result = await Screening2.findByIdAndRemove(req.params.id);
    if (!result) return res.status(400).send('No screening exists under given ID.')
    
    res.send('Screening deleted successfully');
});

router.put("/:id",  async (req, res) => {
        const { seats } = await Screening2.findById(req.params.id);
        const selectedSeats = req.body.selectedSeats;

        for(let i = 0; i < seats.length; i++) {
            selectedSeats.map( item => {
                if(seats[i].row === item[0] && seats[i].seat === item[1]) {
                    seats[i].isOccupied = req.body.isOccupied;
                    seats[i].userID = req.body.isOccupied ? req.user : "";
                }
            });
        }

    const result = await Screening2.findByIdAndUpdate(req.params.id, {
        $set: {
            seats: seats
        }
    }, { new: true });

    if (!result) return res.status(400).send('No screening exists under given ID.');

    res.send(result);
});

module.exports = router;

async function getScreeningRoom2(id){
    const result = ScreeningRoom2.findById(id);
    return result
}






