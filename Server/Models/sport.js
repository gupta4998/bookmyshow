const mongoose = require('mongoose');
const Joi = require('joi');
const ObjectId = mongoose.Schema.Types.ObjectId;

const SportSchema = new mongoose.Schema({
    title: {
        type: String,
        minlength: 1,
        maxlength: 213,
        required: true
    },
    matchDate: {
        type: Date,
        required: true
    },
    genre: {
        type: String, //maybe array? eg. [comedy, action]
        required: true
    },
    location:{
        type:String,
        required:true
    },
    shortDescription: {
        type: String,
        required: true
    },
    ageGroup: {
        type: String,
        required: true
    },
    imageSmall: {
        type: String,
        required:true
    },
    imageLarge: {
        type: String,
        required:true
    },
    screenings: {
        type: Array,
        required:true
    }
});

const Sport = mongoose.model('Sport', SportSchema);

function validateSport(sport) {
    const schema = {
        title: Joi.string().min(1).max(213),
        matchDate: Joi.date(),
        genre: Joi.string().max(50),
        location: Joi.string().max(51),
        shortDescription: Joi.string(),
        ageGroup: Joi.string(),
        imageSmall: Joi.string(),
        imageLarge: Joi.string(),
        
    };

    return Joi.validate(sport, schema)
}

module.exports = {
    Sport: Sport,
    validateSport: validateSport
}