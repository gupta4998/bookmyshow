import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'tachyons';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar1 from './Components/Navbar1';
import Navbar2 from './Components/Navbar2'
import Carousel1 from './Components/Carousel1';
import Sidebar from './Components/Sidebar';
import Movie from './Components/Movie';
import MovieDetail from './Components/MovieDetail'
import Main from './Components/Main';
//import Main1 from './Components/Main1';
import {BrowserRouter, Switch, Route } from 'react-router-dom';
import TicketBuy from './Components/TicketBuy';
import Book from './Components/Book';
import Container from 'react-bootstrap/Container';
import MovieRender from './MovieRender';
import MainEvent from './Events/MainEvent';
import MainSport from './Sports/MainSport';
import Offer from './Offer';
import Gift from './Gift';

function Allcomponent() {
  return (
    <BrowserRouter>
    <div className="App">
   
      {/* <Navbar1/> */}
      <Navbar2/>
      <Switch>
      <Route exact path="/" component={MovieRender} />
      <Route  path="/event" component={MainEvent} />
      <Route  path="/sport" component={MainSport} />
      <Route  path="/offer" component={Offer} />
      <Route  path="/gift" component={Gift} />
      </Switch>
     
      </div>  
    </BrowserRouter>
  );
}

export default Allcomponent;
