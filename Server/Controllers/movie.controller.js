const movies=require('../Models/movie.model');
const mongoose=require('mongoose');
const createError=require('http-errors');

module.exports={
    getAllMovie:async(req,res,next)=>{
        const result=await movies.find();
        res.send(result);
    },
    postAllMovie:async(req,res,next)=>{
        try {
            const movie= new movies(req.body);
            const result=await movie.save();
            res.send(result);
        } catch (error) {
            console.log(error.message);
            if(error.name==='ValidationError'){
                next(createError(422,error.message));
                return;
            }
            next(error);
        }
    },
    getById:async(req,res,next)=>{
        const id=req.params.id;
        try {
            const result=await movies.findById(id);
            if(!result){
                throw createError(404,"No Movie Exist");
            } 
            res.send(result);
        } catch (error) {
            console.log(error.message);
            if(error instanceof mongoose.CastError){
                next(createError(400,"invalid product id"))
                return;
            }
            next(error);
        }
    },
    updateById:async(req,res,next)=>{
        const id=req.params.id;
        const update=req.body;
        const option={new:true};
        try {
            const result=await movies.findByIdAndUpdate(id,update,option);
            res.send(result);
        } catch (error) {
            console.log(error.message);
        }
    
    },
    deleteById:async(req,res,next)=>{
        const id=req.params.id;
        try {
            const result=await movies.findByIdAndDelete(id);
            if(!result){
                throw createError(404,"movies not exist");
            }
            res.send(result);
        } catch (error) {
            console.log(error.message);
            if(error instanceof mongoose.CastError){
                next(createError(400,"invalid product id"))
                return;
            }
            next(error);
            
        }
    },
};