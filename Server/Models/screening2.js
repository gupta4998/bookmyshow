const mongoose = require('mongoose');
const Joi = require('joi');
const ObjectId = mongoose.Schema.Types.ObjectId;

const ScreeningSchema2 = new mongoose.Schema({
    screeningRoomId: {
        type: String,
        required: true
    },
    sportId: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
   
    seats: []
});

const Screening2 = mongoose.model('Screening2', ScreeningSchema2);

function validateScreening2(screening2) {
    const schema = {
        screeningRoomId: Joi.string(),
        sportId: Joi.string(),
        date: Joi.date().min('now')
    };

    return Joi.validate(screening2, schema)
}

module.exports = {
    Screening2: Screening2,
    validateScreening2: validateScreening2
}
