const mongoose = require('mongoose');
const Joi = require('joi');
const ObjectId = mongoose.Schema.Types.ObjectId;

const ScreeningSchema1 = new mongoose.Schema({
    screeningRoomId: {
        type: String,
        required: true
    },
    eventId: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
   
    seats: []
});

const Screening1 = mongoose.model('Screening1', ScreeningSchema1);

function validateScreening1(screening1) {
    const schema = {
        screeningRoomId: Joi.string(),
        eventId: Joi.string(),
        date: Joi.date().min('now')
    };

    return Joi.validate(screening1, schema)
}

module.exports = {
    Screening1: Screening1,
    validateScreening1: validateScreening1
}
