const express = require('express');
const router = express.Router();
const {Event, validateEvent} = require('../models/event');
const {Screening1} = require('../models/screening1');

router.get("/", async (req, res) => {

    const compactRequest = {
        title: 1,
        concertDate: 1,
        genre: 1,
        shortDescription: 1,
        ageGroup: 1,
        imageSmall: 1
    }
    const fullRequest = {
        title: 1,
        concertDate: 1,
        location: 1,
        genre: 1,
        shortDescription: 1,
        fullDescription: 1,
        ageGroup: 1,
        imageSmall: 1,
        imageLarge: 1
    }

    const reqGenre = new RegExp(req.query.genre || /./, "gi");
    const reqAge = new RegExp(req.query.age || /./, "gi");

    const events = await Event.find({genre: reqGenre, ageGroup: reqAge})
        .limit(parseInt(req.query.limit) || 12)
        .sort( { releaseDate: -1 } )
        .select((req.query.size === "compact" ? compactRequest : fullRequest));

    res.send(events);
});

router.get("/:id", async (req, res) => {
    const event = await Event.findById(req.params.id);
    if (!event) return res.status(400).send('No movie exists under given ID.');

    const screenings = await Screening1.find({eventId: event._id})
        .select({
            _id: 1,
            date: 1
        })
    
        event.screenings = screenings;

    res.send(event);
});

router.post("/", async (req, res) => {
    const { error } = validateEvent(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const event = new Event(req.body);

        await event.save();
        res.send(event);
});

router.delete("/:id", async (req, res) => {

    const result = await Event.findByIdAndRemove(req.params.id);
    if (!result) return res.status(400).send('No event exists under given ID.')
    
    res.send('Event deleted successfully');
});

router.put("/:id", async(req, res) => {
    const { error } = validateEvent(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const result = await Event.findByIdAndUpdate(req.params.id, {
        $set: req.body
    }, { new: true });

    if (!result) return res.status(400).send('No movie exists under given ID.');

    res.send(result);
});

module.exports = router;
