import React from 'react';
import Seat2 from './Seat2'

class SeatsInRow2 extends React.Component {
    constructor(props) {
        super(props);


        this.state = {
            seats: this.props.seats
        }
    }
	
	componentWillReceiveProps(nextProps) {
		this.setState({ seats: nextProps.seats }); 
	}



    render() {
        let i = 0;
        const result = this.state.seats.map( item => {
            if(item.seat === "false") {
                i++;
                return <li key={"empty-"+item.row+i} style={{backgroundColor:"#888"}}></li>   
            }

            if(item.isOccupied === true) {
                return (
                    <li 
					key={item.row+item.seat} 
					style={{backgroundColor:"#ac3838"}}>
					{item.row+item.seat}
                    </li>
                )
            }

            return (
                <Seat2 
                    key={item.row+item.seat} 
                    onClick={this.selectSeat}
                    seatRow={item.row}
                    seatNumber={item.seat}
                    selectSeat={this.props.selectSeat}>
                </Seat2>
            )
        });

        return result
    }
}

export default SeatsInRow2;