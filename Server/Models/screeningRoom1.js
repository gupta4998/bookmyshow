const mongoose = require('mongoose');
const Joi = require('joi');
const ObjectId = mongoose.Schema.Types.ObjectId;

const ScreeningRoomSchema1 = new mongoose.Schema({
    city: {
        type: String,
        required: true
    },
    ground: {
        type: String,
        required: true
    },
    screeningRoom: {
        type: String,
        required: true
    },
    seats: {
        type: Object
    }
});

const ScreeningRoom1 = mongoose.model('ScreeningRoom1', ScreeningRoomSchema1);

function validateScreeningRoom1(screeningRoom1) {
    const schema = {
        city: Joi.string(),
        ground: Joi.string(),
        screeningRoom: Joi.string(),
        seats: Joi,
    };

    return Joi.validate(screeningRoom1, schema)
}

module.exports = {
    ScreeningRoom1: ScreeningRoom1,
    validateScreeningRoom1: validateScreeningRoom1
}