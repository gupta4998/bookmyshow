import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'tachyons';
import 'bootstrap/dist/css/bootstrap.min.css';
import Allcomponent from './Allcomponent';
import Book from './Components/Book';
import {BrowserRouter,Route,Switch} from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
    <div className="App">
    
     <Allcomponent/>
     
     
      </div>
      </BrowserRouter>
      
  );
}

export default App;
