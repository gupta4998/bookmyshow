import React from "react";
import "semantic-ui-css/semantic.min.css";
import moment from "moment";

import "./Schedule2.css";
import ScheduleDay2 from "./ScheduleDay2";

const Schedule2 = props => {
  const screeningList = props.screenings.map(screening => {
    return {
      id: screening._id,
      // city: moment(screening.city),
      // cinema: moment(screening.cinema),
      day: moment(screening.date).format("DD.MM.YYYY"),
      hour: moment(screening.date).format("kk:mm")
    };
  });
 // console.log(props.screenings)
  const days = screeningList
    .map(screening => {
      return screening.day;
    })
    .filter((value, index, self) => {
      return self.indexOf(value) === index;
    });

  const screeningDisplay2 = days.map(day => {
    return (
      <ScheduleDay2
      // city={city}
      // cinema={cinema}
        day={day}
        screenings={screeningList}
        loggedIn={props.loggedIn}
        handleLogin={props.handleLogin}
        setUser={props.setUser}
      />
    );
  });
  return (
    <div className="sport-schedule ">
      <h3>Match Schedule</h3>
      {screeningDisplay2}
    </div>
  );
};

export default Schedule2;