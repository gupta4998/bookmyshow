import React from 'react';
import logo from './logo.svg';
import './App.css';
import 'tachyons';
import 'bootstrap/dist/css/bootstrap.min.css';
import Navbar1 from './Components/Navbar1';
import Navbar2 from './Components/Navbar2'
import Carousel1 from './Components/Carousel1';
import Sidebar from './Components/Sidebar';
import Main from './Components/Main';
import {BrowserRouter, Switch, Route } from 'react-router-dom';
import TicketBuy from './Components/TicketBuy';
import Book from './Components/Book';
import Container from 'react-bootstrap/Container';

function MovieRender() {
  return (
    <BrowserRouter>
    <div className="App">
      <Container>
      <Carousel1/>
      </Container>
      <Switch>
      <Route exact path="/carousel" component={TicketBuy} />
      <Route exact path="/book" component={Book} />
      </Switch>
      <hr></hr>
      <div className="row">
        <div className="col-1">
              {/* <Sidebar/> */}
        </div>
        <div className="col-12" bg="dark">
          <Main/>
          
        </div>

      </div>
     
     
    </div>
    </BrowserRouter>
  );
}

export default MovieRender;
