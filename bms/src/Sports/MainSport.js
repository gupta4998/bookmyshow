import React,{Component} from "react";
import "semantic-ui-css/semantic.min.css";
import AppHeader from "../Components/AppHeader";
import SportList from "./SportList";
import SportDetail from "./SportDetail";
class MainSport extends Component {
    constructor(props) {
      super(props);
  
      this.state = { 
        loggedIn: false,
         sportID: "5f0c43d361cfd80a381aff4f",
        username: "Abhishek"
        };
     this.handleLogin = this.handleLogin.bind(this);
      this.handleLogout = this.handleLogout.bind(this);
      this.setSportId = this.setSportId.bind(this);
      this.setUser = this.setUser.bind(this)
    }
  
    handleLogin = () => this.setState({ loggedIn: true });
    handleLogout = () => {
      this.setState({ loggedIn: false });
      document.cookie = "token= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
    };
    setUser = (user) => this.setState({ username: user })
  
    setSportId = (_id) => this.setState({ sportID: _id });
  
    render() {
      console.log(this.state.sportID);
      return (
        <>
         <AppHeader
            loggedIn={this.state.loggedIn}
            handleLogin={this.handleLogin}
            handleLogout={this.handleLogout}
            setUser={this.setUser}
            username={this.state.username}
          /> 
          <div className="main-wrapper mx-3">
            <SportList setSportId={this.setSportId} />

            <SportDetail
              loggedIn={this.state.loggedIn}
              handleLogin={this.handleLogin}
              sportID={this.state.sportID}
              setUser={this.setUser}
            /> 
          
           
      </div>
          
        </>
      );
    }
  }
  
  export default MainSport;
  