import React from "react";
import {} from "semantic-ui-react";
import "semantic-ui-css/semantic.min.css";
import Schedule1 from "./Schedule1";
import basePath from "../api/basePath";
import "./EventDetail.css";
import Button from 'react-bootstrap/Button';
import {Link} from 'react-router-dom';
//import TicketBuy from './TicketBuy';

class EventDetail extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
        _id: "",
        title: "",
        genre: "",
        location: "",
        age: "",
        image: "",
        shortDescription: "",
        screenings: []
      };
    }

  componentDidMount() {
    this.getEvent("5f061645eb754713ecc916d2");
  }

  componentWillReceiveProps(nextProps) {
    this.getEvent(nextProps.eventID);
    console.log(nextProps.eventID)
  }
  

  getEvent = async (id) => {
     await basePath({
      method: "get",
      url: `/api/events/${id}`
    })
      .then(event => {
        this.setState({
          _id: event.data._id,
          title: event.data.title,
          genre: event.data.genre,
         location:event.data.location,
          age: event.data.ageGroup,
          image: event.data.imageSmall,
          shortDescription: event.data.shortDescription,
          screenings: event.data.screenings
        });
      });
    }

  render() {
  console.log("genre",this.state.title)
  //console.log(this.state.screenings)
    return (
      <div className="">
      <div >
         <h2 className="event-title">{this.state.title}</h2>
        <div className="event-details">
          <img
            src={this.state.image}
            alt="poster"
            className="event-details__poster"
          />
          <div className="event-details__main">
            <p className="event-parameter">
              <span>Genre: </span>
              {this.state.genre}
            </p>
            <p className="event-parameter">
              <span>Location: </span>
              {this.state.location} 
            </p>
            <p className="event-parameter">
              <span>Age: </span>
              {this.state.age}
            </p>
            <p className="event-short-description">
              {this.state.shortDescription}
            </p>
            {/* <Button variant="outline-primary"><Link to ="/book">BOOK</Link></Button> */}
            {/* <TicketBuy name="movie__buy" title={this.state.title} /> */}
          </div>
           <Schedule1
            screenings={this.state.screenings}
            loggedIn={this.props.loggedIn}
            handleLogin={this.props.handleLogin}
            setUser={this.props.setUser}
          /> 
          {/* <Button variant="outline-primary">Primary</Button> */}
          {/* <p className="movie-full-description">{this.state.fullDescription}</p>  */}
       </div>
      </div>
      </div>
    );
  }
}

export default EventDetail;