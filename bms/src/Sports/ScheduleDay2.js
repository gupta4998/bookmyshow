import React from "react";
import "semantic-ui-css/semantic.min.css";
import ScreeningRoom2 from "./ScreeningRoom2";
import "./ScheduleDay2.css";

const ScheduleDay2 = props => {
  const dayScreenings = props.screenings.filter(screening => {
    return screening.day === props.day;
  });
  const displayDayButtons = dayScreenings.map(screening => {
      console.log(screening.id)
    return (
      <ScreeningRoom2
        loggedIn={props.loggedIn}
        handleLogin={props.handleLogin}
        screeningId={screening.id}
        text={screening.hour}
        setUser={props.setUser}
      />
    );
  });
  return (
    <div className="schedule-day">
      <p className="schedule-date">{props.day}:</p>
      {displayDayButtons}
    </div>
  );
};

export default ScheduleDay2;
