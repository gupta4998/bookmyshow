import React from 'react';
import "semantic-ui-css/semantic.min.css";
import './Sport.css'

const Sport = (props) => {
    const sports = props.sportinfo.map((info) => {
    return (
        <div key={info._id}>
            <div onClick = {() => {props.setSportId(info._id)}}>
                <img className = "sport_imagesmall" src={info.imageSmall} alt = "movie poster"/>
                <div className = "sport_title">{info.title}</div>
            </div>
        </div>
    )
    });

    return (
        <div className = "sport_sportInfo"> {sports}</div>  
    ) 
};

export default Sport;