const mongoose = require('mongoose');
const Joi = require('joi');
const ObjectId = mongoose.Schema.Types.ObjectId;

const ScreeningRoomSchema2 = new mongoose.Schema({
    city: {
        type: String,
        required: true
    },
    ground: {
        type: String,
        required: true
    },
    screeningRoom: {
        type: String,
        required: true
    },
    seats: {
        type: Object
    }
});

const ScreeningRoom2 = mongoose.model('ScreeningRoom2', ScreeningRoomSchema2);

function validateScreeningRoom2(screeningRoom2) {
    const schema = {
        city: Joi.string(),
        ground: Joi.string(),
        screeningRoom: Joi.string(),
        seats: Joi,
    };

    return Joi.validate(screeningRoom2, schema)
}

module.exports = {
    ScreeningRoom2: ScreeningRoom2,
    validateScreeningRoom2: validateScreeningRoom2
}