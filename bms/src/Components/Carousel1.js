import React,{Component} from 'react';
import Carousel from 'react-bootstrap/Carousel';
import {Link} from 'react-router-dom';
class Carousel1 extends Component{
    render(){
        return(
            <div className="mx-5 mt-3 tc">
                <Carousel>
  <Carousel.Item > <Link to='/carousel'>
    <img
      className="d-block  w-100"
      src="https://i.gadgets360cdn.com/large/thor-ragnarok-poster_1510315046705.jpg"
      alt="First slide"  height="500"
    />
    </Link>
  </Carousel.Item>
  <Carousel.Item> <Link to='/carousel'>
    <img
      className="d-block w-100"
      src="https://cdn.dnaindia.com/sites/default/files/styles/full/public/2019/04/27/817597-avengers-endgame.jpg"
      alt="Third slide" height="500"
    />
     </Link>
  </Carousel.Item>
  <Carousel.Item><Link to='/carousel'>
    <img
      className="d-block w-100"
      src="https://parsi-times.com/wp-content/uploads/2018/07/mi6-fallout_lds.jpg"
      alt="Third slide" height="500"
    />
    </Link>
   
  </Carousel.Item>
</Carousel>

            </div>
        )
    }
}
export default Carousel1;