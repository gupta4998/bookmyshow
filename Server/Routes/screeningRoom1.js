const express = require('express');
const router = express.Router();
const {ScreeningRoom1, validateScreeningRoom1} = require('../models/screeningRoom1');
//const auth = require('../middleware/auth');
const cookieParser = require('cookie-parser');

router.use(cookieParser())

router.get("/", async (req, res) => {
    const screeningRooms1 = await ScreeningRoom1.find()

    res.send(screeningRooms1);
});

router.get("/:id", async (req, res) => {
    const screeningRoom1 = await ScreeningRoom1.findById(req.params.id);
    if (!screeningRoom1) return res.status(400).send('No screeningRoom exists under given ID.');

    res.send(screeningRoom1);
});

router.post("/",  async (req, res) => {
    const { error } = validateScreeningRoom1(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const screeningRoom1 = new ScreeningRoom1(req.body);
    
        await screeningRoom1.save();
        res.send(screeningRoom1);
});

router.delete("/:id",  async (req, res) => {

    const result = await ScreeningRoom1.findByIdAndRemove(req.params.id);
    if (!result) return res.status(400).send('No screeningRoom exists under given ID.')
    
    res.send('ScreeningRoom deleted successfully');
});

router.put("/:id",  async (req, res) => {
	const { error } = validateScreeningRoom1(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const result = await ScreeningRoom1.findByIdAndUpdate(req.params.id, {
        $set: req.body
    }, { new: true });

    if (!result) return res.status(400).send('No screening exists under given ID.');

    res.send(result);
});

module.exports = router;