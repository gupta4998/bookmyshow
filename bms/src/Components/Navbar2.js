import React,{Component} from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Button from 'react-bootstrap/Button';
import NavDropdown from 'react-bootstrap/NavDropdown';
import {Link} from 'react-router-dom';
import Tabs from 'react-bootstrap/Tabs';
//import Sonnet from 'react-bootstrap/Sonnet';
import Tab from 'react-bootstrap/Tab';
class Navbar2 extends Component{
    render(){
        return (
            <div>
                <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Navbar.Brand href="#home"></Navbar.Brand>
                <div className="mt-3">
  <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ7d510fIBhGG89fF8F_YnQlkD369_ZL2PIaA&usqp=CAU" alt="logo" height="75px" width="125px" />
  </div>
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="mr-auto">
      <Nav.Link href="/">Movies</Nav.Link>
      <Nav.Link href="/event">Event</Nav.Link>
      <Nav.Link href="/sport">Sports</Nav.Link>
      
    </Nav>
    <Nav>
      <Nav.Link href="/offer">Offers</Nav.Link>
      <Nav.Link  href="/gift">
       Gift Cards
      </Nav.Link>
    </Nav>
  </Navbar.Collapse>
</Navbar>
            </div>
        )
    }

}
export default Navbar2;