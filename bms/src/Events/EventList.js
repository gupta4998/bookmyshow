import React from 'react';
import axios from 'axios';
import "semantic-ui-css/semantic.min.css";
import './EventList.css';
import Event from './Event';
import basePath from '../api/basePath';



class EventList extends React.Component {

    state = { event_info: [],
       // filterGenre:
           // movie_info2: [],
    };
    
    componentDidMount() {
    this.getEventInfo();
    }

   getEventInfo = async () => {
        const eventresponse = await basePath({
            method: "get",
            url: '/api/events',
            withCredentials: true
        })
        .then(() => {
           basePath({
               method: "get",
               url: 'api/events/?size=compact&limit=12'
           })
            .then(res => {
                const events = res.data.slice(0, 12)
               // const movies2 = res.data.slice(6, 12)
                this.setState({
                    event_info: events,
                //    movie_info2: movies2
                });
            });
        });
    }

    render() {
        return(
            <div className="eventList">
                <div className="eventList_release">
                    <img className="eventList_icon" src="../assets/img/star.svg" alt="icon - star" />
                    <div className="eventList_release_text my-4"> Live Concert </div>
                    <img className="eventList_icon" src="../assets/img/star.svg" alt="icon - star" />
                </div>
                <Event setEventId={this.props.setEventId} eventinfo = {this.state.event_info}/>
                {/* <div className="movieList_today">
                    <div>Today</div>
                </div>
                <Movie setMovieId={this.props.setMovieId} movieinfo = {this.state.movie_info2}/> */}
            </div>   
        );
    }
}

export default EventList;
