import React from "react";
import "semantic-ui-css/semantic.min.css";
import ScreeningRoom1 from "./ScreeningRoom1";
import "./ScheduleDay1.css";

const ScheduleDay1 = props => {
  const dayScreenings = props.screenings.filter(screening1 => {
    return screening1.day === props.day;
  });
  const displayDayButtons = dayScreenings.map(screening1 => {
      console.log(screening1.id)
    return (
      <ScreeningRoom1
        loggedIn={props.loggedIn}
        handleLogin={props.handleLogin}
        screeningId={screening1.id}
        text={screening1.hour}
        setUser={props.setUser}
      />
    );
  });
  return (
    <div className="schedule-day">
      <p className="schedule-date">{props.day}:</p>
      {displayDayButtons}
    </div>
  );
};

export default ScheduleDay1;
