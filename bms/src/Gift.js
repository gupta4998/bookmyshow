import React,{Component} from 'react';
import './Gift.css'
class Gift extends Component{
    render(){
        return(
            <div>
            <div className="gift_image dib mx-3 my-5 ">
                <img src="https://in.bmscdn.com/gv/gift_my_show_27542019095448_480x295.jpg"height="300px" width="350px"/>
                <div className="mx-3"/>
                <img src="https://in.bmscdn.com/gv/gift_my_show_18572019025701_480x295.jpg"/>
                <div className="mx-3"/>
                <img src="https://in.bmscdn.com/gv/gift_my_show_18082019040833_480x295.jpg"/>
                <div className="mx-3"/>
                <img src="https://in.bmscdn.com/gv/gift_my_show_05562019115602_480x295.jpg"/>
                <div className="mx-3"/>
                <img src="https://in.bmscdn.com/gv/gift_my_show_30162019031622_480x295.jpg"/>
                </div>
                <div className="gift_image dib mx-3 my-5">
                <img src="https://in.bmscdn.com/gv/gift_my_show_25412019034153_480x295.jpg"/>
                <div className="mx-3"/>
                <img src="https://in.bmscdn.com/gv/gift_my_show_18482019024856_480x295.jpg"/>
                <div className="mx-3"/>
                <img src="https://in.bmscdn.com/gv/gift_my_show_18432019024353_480x295.jpg"/>
                <div className="mx-3"/>
                <img src="https://in.bmscdn.com/gv/gift_my_show_05202019122040_480x295.jpg"/>
                <div className="mx-3"/>
                <img src="https://in.bmscdn.com/gv/gift_my_show_06572019115735_480x295.jpg"/>
                </div>
            </div>
            
        )
    }
}
export default Gift;