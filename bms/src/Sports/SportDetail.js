import React from "react";
import {} from "semantic-ui-react";
import "semantic-ui-css/semantic.min.css";
import Schedule2 from "./Schedule2";
import basePath from "../api/basePath";
import "./SportDetail.css";
import Button from 'react-bootstrap/Button';
import {Link} from 'react-router-dom';


class SportDetail extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
        _id: "",
        title: "",
        genre: "",
        location: "",
        age: "",
        image: "",
        shortDescription: "",
        screenings: []
      };
    }

  componentDidMount() {
    this.getSport("5f0c43d361cfd80a381aff4f");
  }

  componentWillReceiveProps(nextProps) {
    this.getSport(nextProps.sportID);
  }

  getSport = async (id) => {
     await basePath({
      method: "get",
      url: `/api/sports/${id}`
    })
      .then(sport => {
        this.setState({
          _id: sport.data._id,
          title: sport.data.title,
          genre: sport.data.genre,
          location: sport.data.location,
          age: sport.data.ageGroup,
          image: sport.data.imageSmall,
          shortDescription: sport.data.shortDescription,
          screenings: sport.data.screenings
        });
      });
    }

  render() {
    // console.log(this.state.image)
    // console.log(this.state.screenings)
    return (
      <div className="">
      <div >
         <h2 className="sport-title">{this.state.title}</h2>
        <div className="sport-details">
          <img
            src={this.state.image}
            alt="poster"
            className="sport-details__poster"
          />
          <div className="sport-details__main">
            <p className="sport-parameter">
              <span>Genre: </span>
              {this.state.genre}
            </p>
            <p className="sport-parameter">
              <span>Location: </span>
              {this.state.location} 
            </p>
            {/* <p className="sport-parameter">
              <span>Age: </span>
              {this.state.age}
            </p> */}
            <p className="sport-short-description">
              {this.state.shortDescription}
            </p>
            {/* <Button variant="outline-primary"><Link to ="/book">BOOK</Link></Button> */}
            {/* <TicketBuy name="sport__buy" title={this.state.title} /> */}
          </div>
           <Schedule2
            screenings={this.state.screenings}
            loggedIn={this.props.loggedIn}
            handleLogin={this.props.handleLogin}
            setUser={this.props.setUser}
          /> 
          {/* <Button variant="outline-primary">Primary</Button> */}
          {/* <p className="movie-full-description">{this.state.fullDescription}</p>  */}
       </div>
      </div>
      </div>
    );
  }
}

export default SportDetail;