const express = require('express');
const router = express.Router();
const {Sport, validateSport} = require('../models/sport');
const {Screening2} = require('../models/screening2');

router.get("/", async (req, res) => {

    const compactRequest = {
        title: 1,
        matchDate: 1,
        genre: 1,
        shortDescription: 1,
        ageGroup: 1,
        imageSmall: 1
    }
    const fullRequest = {
        title: 1,
        matchDate: 1,
        location: 1,
        genre: 1,
        shortDescription: 1,
        fullDescription: 1,
        ageGroup: 1,
        imageSmall: 1,
        imageLarge: 1
    }

    const reqGenre = new RegExp(req.query.genre || /./, "gi");
    const reqAge = new RegExp(req.query.age || /./, "gi");

    const sports = await Sport.find({genre: reqGenre, ageGroup: reqAge})
        .limit(parseInt(req.query.limit) || 12)
        .sort( { matchDate: -1 } )
        .select((req.query.size === "compact" ? compactRequest : fullRequest));

    res.send(sports);
});

router.get("/:id", async (req, res) => {
    const sport = await Sport.findById(req.params.id);
    if (!sport) return res.status(400).send('No sport exists under given ID.');

    const screenings = await Screening2.find({sportId: sport._id})
        .select({
            _id: 1,
            date: 1
        })
    
        sport.screenings = screenings;

    res.send(sport);
});

router.post("/", async (req, res) => {
    const { error } = validateSport(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const sport = new Sport(req.body);

        await sport.save();
        res.send(sport);
});

router.delete("/:id", async (req, res) => {

    const result = await Sport.findByIdAndRemove(req.params.id);
    if (!result) return res.status(400).send('No event exists under given ID.')
    
    res.send('Event deleted successfully');
});

router.put("/:id", async(req, res) => {
    const { error } = validateSport(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const result = await Sport.findByIdAndUpdate(req.params.id, {
        $set: req.body
    }, { new: true });

    if (!result) return res.status(400).send('No sport exists under given ID.');

    res.send(result);
});

module.exports = router;
