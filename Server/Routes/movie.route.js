const product=require("express");
const router=product.Router();
const movieController=require('../Controllers/movie.controller');



//GET the data
router.get('/',movieController.getAllMovie);


//POST the data
router.post('/',movieController.postAllMovie);

//GET a single data
router.get('/:id',movieController.getById);

//update the data
router.patch('/:id',movieController.updateById);


//DELETE the data
router.delete('/:id',movieController.deleteById);

module.exports=router;