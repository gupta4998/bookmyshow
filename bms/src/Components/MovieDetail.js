import React from "react";
import {} from "semantic-ui-react";
import "semantic-ui-css/semantic.min.css";
import Schedule from "./Schedule";
import basePath from "../api/basePath";
import "./MovieDetail.css";
import Button from 'react-bootstrap/Button';
import {Link} from 'react-router-dom';
import TicketBuy from './TicketBuy';

class MovieDetail extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
        _id: "",
        title: "",
        genre: "",
        duration: "",
        age: "",
        image: "",
        shortDescription: "",
        fullDescription: "",
        screenings: []
      };
    }

  componentDidMount() {
    this.getMovie("5ef20aa7778c5f2084c42ff2");
  }

  componentWillReceiveProps(nextProps) {
    this.getMovie(nextProps.movieID);
  }

  getMovie = async (id) => {
     await basePath({
      method: "get",
      url: `/api/movies/${id}`
    })
      .then(movie => {
        this.setState({
          _id: movie.data._id,
          title: movie.data.title,
          genre: movie.data.genre,
          duration: movie.data.durationInMinutes,
          age: movie.data.ageGroup,
          image: movie.data.imageSmall,
          shortDescription: movie.data.shortDescription,
          fullDescription: movie.data.fullDescription,
          screenings: movie.data.screenings
        });
      });
    }

  render() {
    console.log(this.state.image)
    console.log(this.state.screenings)
    return (
      <div className="">
      <div >
         <h2 className="movie-title">{this.state.title}</h2>
        <div className="movie-details">
          <img
            src={this.state.image}
            alt="poster"
            className="movie-details__poster"
          />
          <div className="movie-details__main">
            <p className="movie-parameter">
              <span>Genre: </span>
              {this.state.genre}
            </p>
            <p className="movie-parameter">
              <span>Duration: </span>
              {this.state.duration} min
            </p>
            <p className="movie-parameter">
              <span>Age: </span>
              {this.state.age}
            </p>
            <p className="movie-short-description">
              {this.state.shortDescription}
            </p>
            {/* <Button variant="outline-primary"><Link to ="/book">BOOK</Link></Button> */}
            {/* <TicketBuy name="movie__buy" title={this.state.title} /> */}
          </div>
           <Schedule
            screenings={this.state.screenings}
            loggedIn={this.props.loggedIn}
            handleLogin={this.props.handleLogin}
            setUser={this.props.setUser}
          /> 
          {/* <Button variant="outline-primary">Primary</Button> */}
          <p className="movie-full-description">{this.state.fullDescription}</p> 
       </div>
      </div>
      </div>
    );
  }
}

export default MovieDetail;