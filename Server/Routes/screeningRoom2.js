const express = require('express');
const router = express.Router();
const {ScreeningRoom2, validateScreeningRoom2} = require('../models/screeningRoom2');
//const auth = require('../middleware/auth');
const cookieParser = require('cookie-parser');

router.use(cookieParser())

router.get("/", async (req, res) => {
    const screeningRooms2 = await ScreeningRoom2.find()

    res.send(screeningRooms2);
});

router.get("/:id", async (req, res) => {
    const screeningRoom2 = await ScreeningRoom2.findById(req.params.id);
    if (!screeningRoom2) return res.status(400).send('No screeningRoom exists under given ID.');

    res.send(screeningRoom2);
});

router.post("/",  async (req, res) => {
    const { error } = validateScreeningRoom2(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const screeningRoom2 = new ScreeningRoom2(req.body);
    
        await screeningRoom2.save();
        res.send(screeningRoom2);
});

router.delete("/:id",  async (req, res) => {

    const result = await ScreeningRoom2.findByIdAndRemove(req.params.id);
    if (!result) return res.status(400).send('No screeningRoom exists under given ID.')
    
    res.send('ScreeningRoom deleted successfully');
});

router.put("/:id",  async (req, res) => {
	const { error } = validateScreeningRoom2(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const result = await ScreeningRoom2.findByIdAndUpdate(req.params.id, {
        $set: req.body
    }, { new: true });

    if (!result) return res.status(400).send('No screening exists under given ID.');

    res.send(result);
});

module.exports = router;