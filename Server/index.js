const express=require('express');
const app=express();
const mongoose=require('mongoose');
const createError=require('http-errors');
const bodyParser=require('body-parser');
const logger = require('morgan');
const loginRouter = require('./routes/login');
const registrRouter = require('./routes/register');
const movieRouter = require('./routes/movie');
const screeningRouter = require('./routes/screening');
const screeningRoomRouter = require('./routes/screeningRoom');
const eventRouter=require('./routes/event');
const screeningRouter1 = require('./routes/screening1');
const screeningRoomRouter1 = require('./routes/screeningRoom1');
const sportRouter=require('./routes/sport');
const screeningRouter2 = require('./routes/screening2');
const screeningRoomRouter2 = require('./routes/screeningRoom2');
const path = require('path');
app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
const config = require('./config/default.json');
const cors=require('cors');
const db = config.db;
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(logger('tiny'));
mongoose.connect(db,{
    useNewUrlParser:true,
    useUnifiedTopology:true,
    useFindAndModify:false
})
.then(()=>{
    console.log('mongoose is connected...');
})
app.use(cors({ 
    credentials: true,
    origin: true
 }));

const movie=require('./Routes/movie.route');
app.use('/movie',movie);

const image=require('./Routes/image.route');
app.use('/image',image);
app.use('/login', loginRouter);
app.use('/register', registrRouter);
app.use('/api/movies', movieRouter);
app.use('/api/screenings', screeningRouter);
app.use('/api/screeningrooms', screeningRoomRouter);
app.use('/api/events', eventRouter);
app.use('/api/screenings1', screeningRouter1);
app.use('/api/screeningrooms1', screeningRoomRouter1);
app.use('/api/sports', sportRouter);
app.use('/api/screenings2', screeningRouter2);
app.use('/api/screeningrooms2', screeningRoomRouter2);


//error control
app.use((req,res,next)=>{
    next(createError(404,'Not Found'));
});

app.use((err,req,res,next)=>{
    res.status(err.status||500)
    res.send({
        error:{
            status:err.status||500,
            message:err.message
        }
    });
})

app.listen(4000,()=>{
    console.log("server is running at port 4000...");
})
