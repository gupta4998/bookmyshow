import React from "react";
import "semantic-ui-css/semantic.min.css";
import AppHeader from "./AppHeader";
import MovieList from "./MovieList";
import MovieDetail from "./MovieDetail";

class Main extends React.Component {
    constructor(props) {
      super(props);
  
      this.state = { 
        loggedIn: false,
         movieID: "5ef20aa7778c5f2084c42ff2",
        username: "Abhishek"
        };
      this.handleLogin = this.handleLogin.bind(this);
      this.handleLogout = this.handleLogout.bind(this);
      this.setMovieId = this.setMovieId.bind(this);
      this.setUser = this.setUser.bind(this)
    }
  
    handleLogin = () => this.setState({ loggedIn: true });
    handleLogout = () => {
      this.setState({ loggedIn: false });
      document.cookie = "token= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
    };
    setUser = (user) => this.setState({ username: user })
  
    setMovieId = (_id) => this.setState({ movieID: _id });
  
    render() {
      return (
        <>
          <AppHeader
            loggedIn={this.state.loggedIn}
            handleLogin={this.handleLogin}
            handleLogout={this.handleLogout}
            setUser={this.setUser}
            username={this.state.username}
          />
          <div className="main-wrapper mx-3">
            <MovieList setMovieId={this.setMovieId} />

            <MovieDetail
              loggedIn={this.state.loggedIn}
              handleLogin={this.handleLogin}
              movieID={this.state.movieID}
              setUser={this.setUser}
            /> 
          
           
      </div>
          
        </>
      );
    }
  }
  
  export default Main;
  