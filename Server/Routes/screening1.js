const express = require('express');
const router = express.Router();
const {Screening1, validateScreening1} = require('../models/screening1');
const {ScreeningRoom1, validateScreeningRoom1} = require('../models/screeningRoom1');
const auth = require('../middleware/auth');
const cookieParser = require('cookie-parser');

router.use(cookieParser())

router.get("/", async (req, res) => {
    const screenings1 = await Screening1.find()

    res.send(screenings1);
});

router.get("/:id", async (req, res) => {
    const screening1 = await Screening1.findById(req.params.id);
    if (!screening1) return res.status(400).send('No screening exists under given ID.');

    res.send(screening1);
});

router.post("/",  async (req, res) => {
    const { error } = validateScreening1(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const screening1 = new Screening1(req.body);
    const screeningRoom1 = await getScreeningRoom1(req.body.screeningRoomId);
     console.log(screeningRoom1.seats);
    const arr = Object.entries(screeningRoom1.seats)
    console.log(Object.entries(screeningRoom1.seats))

    const myHeadHurts =[]
    
    
    for(let i = 0; i < arr.length; i++) {
        for(seat of arr[i][1]) {
            if(arr[i][0]==="F") break;
            if(Number.parseInt(arr[i][0]) > 10) continue;
            myHeadHurts.push({
                row: arr[i][0],
                seat: seat,
                isOccupied: false,
                userID: ""
            });
        }
    };

    screening1.seats = myHeadHurts;
    screening1.city = screeningRoom1.city;
    screening1.ground=screeningRoom1.ground;

        await screening1.save();
        res.send(screening1);
});

router.delete("/:id",  async (req, res) => {

    const result = await Screening1.findByIdAndRemove(req.params.id);
    if (!result) return res.status(400).send('No screening exists under given ID.')
    
    res.send('Screening deleted successfully');
});

router.put("/:id",  async (req, res) => {
        const { seats } = await Screening1.findById(req.params.id);
        const selectedSeats = req.body.selectedSeats;

        for(let i = 0; i < seats.length; i++) {
            selectedSeats.map( item => {
                if(seats[i].row === item[0] && seats[i].seat === item[1]) {
                    seats[i].isOccupied = req.body.isOccupied;
                    seats[i].userID = req.body.isOccupied ? req.user : "";
                }
            });
        }

    const result = await Screening1.findByIdAndUpdate(req.params.id, {
        $set: {
            seats: seats
        }
    }, { new: true });

    if (!result) return res.status(400).send('No screening exists under given ID.');

    res.send(result);
});

module.exports = router;

async function getScreeningRoom1(id){
    const result = ScreeningRoom1.findById(id);
    return result
}






