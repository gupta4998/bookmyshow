import React from "react";
import basePath from "../api/basePath";
import { Button, Container, Header, Modal } from "semantic-ui-react";
import "semantic-ui-css/semantic.min.css";
import moment from "moment";
import Rows2 from "./Rows2";
import ScreeningRoomSubmit2 from "./ScreeningRoomSubmit2";
import "./ScreeningRoom2.css";
import LoginForm from "../Components/LoginForm";

class ScreeningRoom2 extends React.Component {
  constructor(props) {
    super(props);

    this.seatElement = React.createRef();

    this.state = {
      id: this.props.screeningId,
      screeningRoomId: "",
      sportId: "",
      date: "",
      sportTitle: "",
      seats: [],
      selectedSeats: []
    };

    this.selectSeat = this.selectSeat.bind(this);
    this.resetSeats = this.resetSeats.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
    this.handleLoginClose = this.handleLoginClose.bind(this);
  }

  resetSeats(seats) {
    this.setState({ seats: seats });
    this.seatElement.current.setState({ seats: seats });
  }

  componentDidMount() {
    this.getScreening();
  }

  handleLogin = () => {
    this.props.handleLogin();
  };

  handleLoginClose = () => this.setState({ loginModalOpen: false });

  ifLogged() {
    if (this.props.loggedIn) {
        console.log("abc",this.state.id)
        console.log(this.state.sportId)
        console.log(this.state.screeningRoomId)
      return (
        <>
          <Header as="h2" textAlign="center">
            {this.state.sportTitle}
            <Header.Subheader>{this.state.date}</Header.Subheader>
          </Header>
          <Modal.Content>
            <Container text className="screeningRoom">
              <Rows2
                seats={this.state.seats}
                selectSeat={this.selectSeat}
                ref={this.seatElement}
              />
              <ScreeningRoomSubmit2
                screeningId={this.state.id}
                selectedSeats={this.state.selectedSeats}
                resetSeats={this.resetSeats}
              />
            </Container>
          </Modal.Content>
        </>
      );
    } 
    else {
      return (
            <LoginForm style={{ padding: '20%'}}
            handleLogin={this.handleLogin}
            handleClose={this.handleLoginClose}
            setUser={this.props.setUser}
            />
        
      );
    }
  }
  render() {
    return (
      <Modal
        trigger={
          <Button
            onClick={() => {
              this.setState({ selectedSeats: [] });
              this.getScreening() }}>
            {this.props.text}
          </Button>
        }
        closeIcon>
      {this.ifLogged()}
      </Modal>
    );
  }

  getScreening = async () => {
    const reservationResponse = await basePath({
      method: "get",
      url: `/api/screenings2/${this.props.screeningId}`,
      withCredentials: true
    })
      .then(res => {
        this.setState({
          id: res.data._id,
          screeningRoomId: res.data.screeningRoomId,
          sportId: res.data.sportId,
          date: moment(res.data.date)
            .format("MMMM Do YYYY, h:mm:ss")
            .toString(),
          seats: res.data.seats
        });
      })
      basePath({
        method: "get",
        url: `/api/sports/${this.state.sportId}`,
      })
          .then(res => {
            this.setState({
              sportTitle: res.data.title
            });
          });
    }

  selectSeat(row, seat) {
    const seats = this.state.selectedSeats;
    let index = seats.findIndex(item => {
      return item.toString() === [row, seat].toString();
    });
    if (index !== -1) seats.splice(index, 1);
    else seats.push([row, seat]);

    this.setState({
      selectedSeats: seats
    });
  }
}

export default ScreeningRoom2;
