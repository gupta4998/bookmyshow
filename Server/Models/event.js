const mongoose = require('mongoose');
const Joi = require('joi');
const ObjectId = mongoose.Schema.Types.ObjectId;

const EventSchema = new mongoose.Schema({
    title: {
        type: String,
        minlength: 1,
        maxlength: 213,
        required: true
    },
    concertDate: {
        type: Date,
        required: true
    },
    genre: {
        type: String, //maybe array? eg. [comedy, action]
        required: true
    },
    location:{
        type:String,
        required:true
    },
    shortDescription: {
        type: String,
        required: true
    },
    ageGroup: {
        type: String,
        required: true
    },
    imageSmall: {
        type: String,
        required:true
    },
    imageLarge: {
        type: String,
        required:true
    },
    screenings: {
        type: Array,
        required:true
    }
});

const Event = mongoose.model('Event', EventSchema);

function validateEvent(event) {
    const schema = {
        title: Joi.string().min(1).max(213),
        concertDate: Joi.date(),
        genre: Joi.string().max(50),
        location: Joi.string().max(51),
        shortDescription: Joi.string(),
        ageGroup: Joi.string(),
        imageSmall: Joi.string(),
        imageLarge: Joi.string(),
        
    };

    return Joi.validate(event, schema)
}

module.exports = {
    Event: Event,
    validateEvent: validateEvent
}