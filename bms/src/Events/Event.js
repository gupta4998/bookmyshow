import React from 'react';
import "semantic-ui-css/semantic.min.css";
import './Event.css'

const Event = (props) => {
    const events = props.eventinfo.map((info) => {
    return (
        <div key={info._id}>
            <div onClick = {() => {props.setEventId(info._id)}}>
                <img className = "event_imagesmall" src={info.imageSmall} alt = "movie poster"/>
                <div className = "event_title">{info.title}</div>
            </div>
        </div>
    )
    });

    return (
        <div className = "event_eventInfo"> {events}</div>  
    ) 
};

export default Event;