const mongoose=require('mongoose');
const Schema=mongoose.Schema;
const movieSchema=new Schema({
   
    movie:{
        type:String,
        required:true
    },
    Time:{
        type:String,
        required:true
    },
    Rating:{
        type:Number,
        required:true
    }
});

const movie=mongoose.model('movie',movieSchema);
module.exports=movie;