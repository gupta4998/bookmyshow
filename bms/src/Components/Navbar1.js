import React,{Component} from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/Form';
import NavDropdown from 'react-bootstrap/NavDropdown';
import Auths from './Auths';
class Navbar1 extends Component{
    render(){
        return(
            <div>
                <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
  <Navbar.Brand href="#home"></Navbar.Brand>
  <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQ7d510fIBhGG89fF8F_YnQlkD369_ZL2PIaA&usqp=CAU" alt="logo" height="75px" width="125px" />
  {/* <h1 className="mx-3">BookMyShow</h1> */}
  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
  <Navbar.Collapse id="responsive-navbar-nav">
    <Nav className="mr-auto">
    <Form inline>
    <FormControl type="text" placeholder="Search" className=" mr-sm-2 " />
    <Button type="submit" variant="outline-info">Submit</Button>
  </Form>
      <Nav.Link href="#features">Features</Nav.Link>
      <Nav.Link href="#pricing">Pricing</Nav.Link>
      {/* <NavDropdown title="Dropdown" id="collasible-nav-dropdown">
        <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
        <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
      </NavDropdown> */}
    </Nav>
    <Nav>
    <NavDropdown title="Location" id="collasible-nav-dropdown">
        <NavDropdown.Item href="#action/3.1">Delhi-NCR</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.2">Kanpur</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.3">Lucknow</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.3">Mumbai</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.3">Banglore</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.3">Pune</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.3">Hydrabad</NavDropdown.Item>
        {/* <NavDropdown.Divider />
        <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item> */}
      {/* </NavDropdown>
       <NavDropdown title="Language" id="collasible-nav-dropdown">
        <NavDropdown.Item href="#action/3.1">English</NavDropdown.Item>
        <NavDropdown.Item href="#action/3.2">Hindi</NavDropdown.Item>
         <NavDropdown.Divider />
      <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item> */}
     
      </NavDropdown> 
       {/* <Auths/> */}
      {/* <Nav.Link href="#deets">More deets</Nav.Link>  */}
      
    </Nav>
  </Navbar.Collapse>
</Navbar>
            </div>
        )
    }
}
export default Navbar1;