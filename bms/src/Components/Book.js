import React,{Component} from 'react';
import { Button, Modal } from "semantic-ui-react";
import Popup from "reactjs-popup";
class Book extends Component{
    render(){
        return (
            <Popup>
            <div>
                 <Modal.Header>Reservation successful!</Modal.Header>
            <Modal.Content>
              <p>Thank you for using our website.</p>
            </Modal.Content>
            </div>
            </Popup>
        )
    }
}
export default Book;