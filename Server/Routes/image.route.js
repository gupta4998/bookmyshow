const product=require("express");
const router=product.Router();
 const image=require('../Models/image.model');
const multer = require('multer');


const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads/');
  },
  filename: function(req, file, cb) {
    cb(null, new Date().getTime() +'-'+ file.originalname);
  }
});
const upload= multer({storage: storage})

router.get("/", (req, res, next) => {
    image.find()
      .select("movieImage Movie")
      .exec()
      .then(docs => {
        const response = {
          count: docs.length,
          images: docs.map(doc => {
            return {
              movieImage: doc.movieImage,
              Movie: doc.Movie,
              _id: doc._id,
              request: {
                type: "GET",
                url: "http://localhost:4000/image/" + doc._id
              }
            };
          })
        };
        res.status(200).json(response);
    })
    .catch(err=>{
        console.log(err);
      res.status(500).json({
        error: err
      });
    })
}) 

router.post('/',upload.single('movieImage'),(req,res,next)=>{
    
        const Image=new image({
            movieImage:req.file.path,
            Movie:req.body.Movie
        });
        Image.save()
        .then(result=>{
            console.log(result)
      res.status(201).json({
        message: "Created product successfully",
        createdProduct: {
            Movie: result.Movie
        }
        });
    }).catch (error=>{
        console.log(error.message);
        if(error.name==='ValidationError'){
            next(createError(422,error.message));
            return;
        }
        next(error);
    })

});
    

module.exports=router;