import React,{Component} from "react";
import "semantic-ui-css/semantic.min.css";
import AppHeader from "../Components/AppHeader";
import EventList from "./EventList";
import EventDetail from "./EventDetail";
class MainEvent extends Component {
    constructor(props) {
      super(props);
  
      this.state = { 
        loggedIn: false,
         eventID: "5f061645eb754713ecc916d2",
        username: "Abhishek"
        };
     this.handleLogin = this.handleLogin.bind(this);
      this.handleLogout = this.handleLogout.bind(this);
      this.setEventId = this.setEventId.bind(this);
      this.setUser = this.setUser.bind(this)
    }
  
    handleLogin = () => this.setState({ loggedIn: true });
    handleLogout = () => {
      this.setState({ loggedIn: false });
      document.cookie = "token= ; expires = Thu, 01 Jan 1970 00:00:00 GMT";
    };
    setUser = (user) => this.setState({ username: user })
  
    setEventId = (_id) => this.setState({ eventID: _id });
  
    render() {
      console.log(this.state.eventID);
      return (
        <>
         <AppHeader
            loggedIn={this.state.loggedIn}
            handleLogin={this.handleLogin}
            handleLogout={this.handleLogout}
            setUser={this.setUser}
            username={this.state.username}
          /> 
          <div className="main-wrapper mx-3">
            <EventList setEventId={this.setEventId} />

            <EventDetail
              loggedIn={this.state.loggedIn}
              handleLogin={this.handleLogin}
              eventID={this.state.eventID}
              setUser={this.setUser}
            /> 
          
           
      </div>
          
        </>
      );
    }
  }
  
  export default MainEvent;
  