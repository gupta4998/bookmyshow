import React,{Component} from 'react';
import Dropdown from 'react-bootstrap/Dropdown';
class Sidebar extends Component{
    render(){
        return (
            <div className=" dib  "> 
                 <div class="underline green "> <p1>Select Language</p1></div>
      <form class="pa1">
  
    <div class="flex items-center mb1 link dim  black">
      <input class="mr2" type="checkbox" id="spacejam" value="spacejam"/>
      <label for="spacejam" class="lh-copy">English</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="airbud" value="airbud"/>
      <label for="airbud" class="lh-copy">Hindi</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="hocuspocus" value="hocuspocus"/>
      <label for="hocuspocus" class="lh-copy">Telgue</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="diehard" value="diehard"/>
      <label for="diehard" class="lh-copy">Tamil</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="primer" value="primer"/>
      <label for="primer" class="lh-copy">Malyalam</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="proxy" value="proxy"/>
      <label for="proxy" class="lh-copy">Kananda</label>
    </div>
   
</form>  
        <hr></hr>
        <div class="underline green"> <p1>Genre</p1></div>
      <form class="pa1">
  
    <div class="flex items-center mb1 link dim  black">
      <input class="mr2" type="checkbox" id="spacejam" value="spacejam"/>
      <label for="spacejam" class="lh-copy">Action</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="airbud" value="airbud"/>
      <label for="airbud" class="lh-copy">Horror</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="hocuspocus" value="hocuspocus"/>
      <label for="hocuspocus" class="lh-copy">Drama</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="diehard" value="diehard"/>
      <label for="diehard" class="lh-copy">Adventure</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="primer" value="primer"/>
      <label for="primer" class="lh-copy">Science Fiction</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="proxy" value="proxy"/>
      <label for="proxy" class="lh-copy">Crime</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="homealone" value="homealone"/>
      <label for="homealone" class="lh-copy">Comedy</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="homealone" value="homealone"/>
      <label for="homealone" class="lh-copy">Biography</label>
    </div>
    <div class="flex items-center mb2 link dim  black">
      <input class="mr2" type="checkbox" id="homealone" value="homealone"/>
      <label for="homealone" class="lh-copy">Animination</label>
    </div>
</form>

            </div>
        )
    }
}
export default Sidebar;