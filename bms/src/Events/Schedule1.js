import React from "react";
import "semantic-ui-css/semantic.min.css";
import moment from "moment";

import "./Schedule1.css";
import ScheduleDay1 from "./ScheduleDay1";

const Schedule1 = props => {
  const screeningList = props.screenings.map(screening1 => {
    return {
      id: screening1._id,
      // city: moment(screening.city),
      // cinema: moment(screening.cinema),
      day: moment(screening1.date).format("DD.MM.YYYY"),
      hour: moment(screening1.date).format("kk:mm")
    };
  });
 // console.log(props.screenings)
  const days = screeningList
    .map(screening1 => {
      return screening1.day;
    })
    .filter((value, index, self) => {
      return self.indexOf(value) === index;
    });

  const screeningDisplay1 = days.map(day => {
    return (
      <ScheduleDay1
      // city={city}
      // cinema={cinema}
        day={day}
        screenings={screeningList}
        loggedIn={props.loggedIn}
        handleLogin={props.handleLogin}
        setUser={props.setUser}
      />
    );
  });
  return (
    <div className="event-schedule ">
      <h3>Concert Schedule</h3>
      {screeningDisplay1}
    </div>
  );
};

export default Schedule1;